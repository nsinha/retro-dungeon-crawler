(function(){
	var tileWidth = 32;
	function start(){
		var frame = 0;
		room = new Room(25, 20, tileWidth)
		room.loadMap(mapData);
		   setInterval(function () {
            draw();
            step(frame++);
        }, 17);
	}
	var canvas = document.getElementById("game");
	var ctx = canvas.getContext('2d');
 	var room;
 	var images = {};
 	var player;
 	var movingLeft = false;
 	var movingUp = false;
 	var movingDown = false;
 	var movingRight = false;

 	var mapData = {
	 	defaultTile: {
	 		tile: 'grass',
	 		walkable: true
	 	},
	 	tiles:[]
 	};
 	function setup(){

		var i;
 		for(i = 0;  i < 25; ++i){
 			mapData.tiles.push({
 				x: i,
 				y: 0,
 				tile: 'water1',
 				walkable:false
 			});

 			mapData.tiles.push({
 				x: i,
 				y: 19,
 				tile: 'water1',
 				walkable:false
 			});
 		}

 		for(i = 0;  i < 20; ++i){
 			mapData.tiles.push({
 				x: 0,
 				y: i,
 				tile: 'water1',
 				walkable:false
 			});

 			mapData.tiles.push({
 				x: 24,
 				y: i,
 				tile: 'water1',
 				walkable:false
 			});
 		}


 		var grass = new Image();
		grass.src = 'content/images/grass.png';
        images["grass"] = grass;

        var water1 = new Image();
		water1.src = 'content/images/water1.png';
        images["water1"] = water1;

        var playerImg = new Image();
		playerImg.src = 'content/images/player.png';
        images["player"] = playerImg;

        player = new Player(400, 500);

        document.addEventListener( "keydown", doKeyDown, true);
        document.addEventListener( "keyup", doKeyUp, true);

        window.addEventListener("gamepadconnected", function(e) {
		  console.log("Gamepad connected at index %d: %s. %d buttons, %d axes.",
		    e.gamepad.index, e.gamepad.id,
		    e.gamepad.buttons.length, e.gamepad.axes.length);
		});

 	}

 	function draw() {
 		
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.fillStyle = "black";
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        for(var i = 0; i < 25; ++i){
        	for(var j = 0; j < 20; ++j){
        		var cell = room.getCell(i, j);
        		image = images[cell.tile];
        		ctx.drawImage(image, i*tileWidth, j*tileWidth);
        	}
        }
        ctx.drawImage(images["player"], player.x, player.y);
        
    }

    function step(frameNumber){
    	var moveSpeed = 3;
    	var pos;
    	if(movingUp){
    		pos = player.tryMove(0, -moveSpeed);
    		if(room.isWalkable(pos.x, pos.y, tileWidth, tileWidth))
    		{
    			player.move(0, -moveSpeed);
    		}
    	}
    	if(movingDown){
    		pos = player.tryMove(0, moveSpeed);
    		if(room.isWalkable(pos.x, pos.y, tileWidth, tileWidth))
    		{
    			player.move(0, moveSpeed);
    		}
    	}

    	if(movingLeft){
    		pos = player.tryMove(-moveSpeed, 0);
    		if(room.isWalkable(pos.x, pos.y, tileWidth, tileWidth))
    		{
    			player.move(-moveSpeed, 0);
    		}

    	}
    	if(movingRight){
    		pos = player.tryMove(moveSpeed, 0);
    		if(room.isWalkable(pos.x, pos.y, tileWidth, tileWidth))
    		{
    			player.move(moveSpeed, 0);
    		}
    	}
    }

    function doKeyDown(e){
    	
    	if ( e.keyCode == 87 ) {
    		movingUp = true;
		}

		if ( e.keyCode == 83 ) {
			movingDown = true;
		}

		if ( e.keyCode == 65 ) {
			movingLeft = true;
		}

		if ( e.keyCode == 68 ) {
			movingRight = true;
		}
    }

    function doKeyUp(e){
    	
    	if ( e.keyCode == 87 ) {
    		movingUp = false;
		}

		if ( e.keyCode == 83 ) {
			movingDown = false;
		}

		if ( e.keyCode == 65 ) {
			movingLeft = false;
		}

		if ( e.keyCode == 68 ) {
			movingRight = false;
		}
    }

    setup();
    start();
})();