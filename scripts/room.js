function Room(width, height, cellWidth){
	this.width = width;
	this.height = height;
	this.cells = new Array(this.width);
	this.cellWidth = cellWidth;
	for(var i = 0; i < this.width; ++i){
		this.cells[i] = new Array(this.height);
	}
}

Room.prototype.loadMap = function(mapObj){
	var x, y;
	for(x = 0; x < this.width; ++x){
		for(y = 0; y < this.height; ++y){
			this.cells[x][y] = {
				x: x,
				y: y,
				tile: mapObj.defaultTile.tile,
				walkable: mapObj.defaultTile.walkable
			}
		}
	}

	var length = mapObj.tiles.length;
	var i;
	for(i = 0; i < length; ++i){
		var cellData = mapObj.tiles[i];
		this.cells[cellData.x][cellData.y] = {
				x: cellData.x,
				y: cellData.y,
				tile: cellData.tile,
				walkable: cellData.walkable
			}
	}
}

Room.prototype.getWidth = function(){
	return this.width;
}

Room.prototype.getHeight = function(){
	return this.height;
}

Room.prototype.getCell = function(x, y){
	return this.cells[x][y];
}

Room.prototype.isWalkable = function(x, y, width, height){
	var startX = Math.floor(x/this.cellWidth);
	var startY = Math.floor(y/this.cellWidth);

	var endX = Math.floor((x + width)/this.cellWidth);
	var endY = Math.floor((y + height)/this.cellWidth);

	var i, j;

	for(i = startX; i<= endX; ++i){
		for(j = startY; j <= endY; ++j){
			if(!this.cells[i][j].walkable){
				return false;
			}
		}
	}
	return true;
}