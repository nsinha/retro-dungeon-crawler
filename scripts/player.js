function Player(x, y){
	this.x = x;
	this.y = y;
}

Player.prototype.getX = function(){
	return this.x;
}

Player.prototype.getY = function(){
	return this.y;
}

Player.prototype.tryMove = function(dx, dy){
	return {
			x: this.x + dx,
			y: this.y + dy
		}
}

Player.prototype.move = function(dx, dy){
	this.x += dx;
	this.y += dy;
}